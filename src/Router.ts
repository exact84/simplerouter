import * as http from 'http';

export type Handler<T> = (request: Request<T>, response: Response) => void;

export enum HTTPMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}

interface Request<T = any> extends http.IncomingMessage {
    body?: T;   
}   

type Response = http.ServerResponse;

export class Router {

    private routes: { [key: string]: { [method: string]: Handler<any> } } = {};

    private addRoute<T>(method: string, path: string, handler: Handler<T>): void {
        if (!this.routes[path]) {   
            this.routes[path] = {};
        }
        if (this.routes[path][method]) {    
            throw new Error(`Handler for ${method} ${path} already specified`);
        }
        this.routes[path][method] = handler; // добавляем обработчик
    }

    get<T>(path: string, handler: Handler<T>): this {
        this.addRoute(HTTPMethod.GET, path, handler);
        return this;
    }

    post<T>(path: string, handler: Handler<T>): this {
        this.addRoute(HTTPMethod.POST, path, handler);
        return this;
    }

    put<T>(path: string, handler: Handler<T>): this {
        this.addRoute(HTTPMethod.PUT, path, handler);
        return this;
    }

    delete<T>(path: string, handler: Handler<T>): this {
        this.addRoute(HTTPMethod.DELETE, path, handler);
        return this;
    }

    // Разбор тела запроса
    private parseBody<T>(request: http.IncomingMessage): Promise<T> {
        return new Promise((resolve, reject) => {
            let body = '';
            request.on('data', chunk => {   // Разбираем поток запроса,
                body += chunk;    
            });
            request.on('end', () => {
                try {
                    const parsedBody = JSON.parse(body);
                    resolve(parsedBody);
                } catch (error) {
                    reject(error);
                }
            });
            request.on('error', err => {
                reject(err);
            });
        });
    }

    getHandler(): (request: Request, response: Response) => Promise<void> {
        return async (request: Request, response: Response) => {
            const method = request.method;
            const url = request.url;

            if (!method || !url) {  
                response.statusCode = 400;
                response.end('Bad Request');
                return;
            }

            const route = this.routes[url];
            const handler = route ? route[method] : null;
           
            if (handler) {  
                try {   
                    if (method === 'POST' || method === 'PUT') {
                        request.body = await this.parseBody(request);
                    }
                    handler(request, response);
                } catch (error) {  
                    response.statusCode = 500;
                    response.end('Internal Server Error');
                }
            } else {  
                response.statusCode = 404;
                response.end('Not Found');
            }
        };
    }
}
